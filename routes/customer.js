const express = require('express');
const customerController = require('../controllers/customer');
const router = express.Router();

router.get('/', customerController.getAll);
router.get('/:id', customerController.getById);
router.post('/', customerController.new);
router.put('/:id', customerController.update);
router.delete('/:id', customerController.remove);

router.get('/search/:text', customerController.search);
module.exports = router;
