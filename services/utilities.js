exports.returnData = (res,data, message = "Se proceso la petición correctamente.") => {
  res.json({
    success: true,
    message,
    data
  })
}
 
exports.returnError = (res,error,message = "Existe un error al procesar su petición.", status = 400) => {
  res.status(status)
  res.json({
    success: false,
    message,
    error
  })
}

exports.prepareText = text => {
  return text.trim().replace(/\s+/g, " ");
};
