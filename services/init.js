const customerController = require('../controllers/customer')
const dealershipController = require('../controllers/dealership')
const userController = require('../controllers/user')

module.exports = async () => {
    userController.loadJson()
    await dealershipController.loadJson()
    customerController.loadJson()
}