const utility = require('./utilities')
const jwt = require('jsonwebtoken')
const publicURL = {'/api/v1/login': true}

exports.validation = (req, res, next) => {
    if(publicURL[req.originalUrl]){
        next()
    } else {
        try {
            const token = req.headers.authorization.replace("Token ", "")
            let user = jwt.verify(token, process.env.PASSJWT);
            req.user = user
            next()
        } catch (err) {
            utility.returnError(res,"INVALID-TOKEN")
        }
    }
}