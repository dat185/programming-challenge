# Programming Challenge - CONSULTEC

## Running MongoDB with docker

```
docker pull mongo
sudo docker run -d -p 27017:27017 -v ~/data:/data/db mongo
```
Using console

```
docker ps //to get the container_id
docker exec -it <container_id> bash
```
After you enter to the root session enter this:

```
mongo
```

## Nodejs and dependencies

```
npm install
```

After installation 

```
npm start
```

## Front-end Project

Folder: /front-end/

## Endpoints

* Documentation:
**localhost:8080/api-docs/**

* Front-end: **localhost:8080/**
