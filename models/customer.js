const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt-nodejs');

let CustomerSchema = new Schema({
  name: {
    type: String,
    index: true,
    required: true
  },
  last_name: {
    type: String,
    index: true,
    required: true
  },
  username: {
    type: String,
    unique: true,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  email: {
    type: String,
    unique: true,
    required: true
  },
  address: String,
  dealership: { type: Schema.Types.ObjectId, ref: 'dealership', required: true, index:true },
  status: {
    type: String,
    enum: ['ENABLED', 'DISABLED'],
    default: 'ENABLED'
  },
  date_created: { type: Date, default: Date.now }
},{versionKey:false});

CustomerSchema.pre('save', function(next) {
  var customer = this;

  if (!customer.isModified('password')) return next();
  bcrypt.genSalt(8, (err, salt) =>{
      if (err) return next(err);
      bcrypt.hash(customer.password, salt,null, (err, hash) =>{
          if (err) return next(err);
          customer.password = hash;
          next();
      });
  });
});

let Customer = mongoose.model('customer', CustomerSchema);
module.exports = Customer;