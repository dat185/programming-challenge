const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt-nodejs');

let UserSchema = new Schema({
  username: {
    type: String,
    unique: true,
    required: true
  },
  password: {
    type: String,
    required: true
  }
},{versionKey: false});

UserSchema.pre('save', function(next) {
  var user = this;
  if (!user.isModified('password')) return next();

  bcrypt.genSalt(8, (err, salt) =>{
      if (err) return next(err);
      bcrypt.hash(user.password, salt,null, (err, hash) =>{
          if (err) return next(err);
          user.password = hash;
          next();
      });
  });
});

UserSchema.methods.comparePassword = (userPassword, hastPassword, cb) => {
  bcrypt.compare(userPassword, hastPassword, (err, isMatch) => {
      if (err) return cb(err);
      cb(null, isMatch);
  });
}

let User = mongoose.model('user', UserSchema);
module.exports = User;