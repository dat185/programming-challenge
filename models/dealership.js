const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let DealershipSchema = new Schema({
  _id: Schema.Types.ObjectId,
  name: {
    type: String,
    unique: true,
    required: true
  },
  name_code: {
    type: String,
    required: true
  }
},{versionKey: false});

let Dealership = mongoose.model('Dealership', DealershipSchema);
module.exports = Dealership;