const fs = require('fs');
const User = require('../models/user');
const utility = require('../services/utilities');
const jwt = require('jsonwebtoken');

exports.login = async (req, res) => {
  try {
    const user = await User.findOne({ username: req.body.username })
    if (user)
      user.comparePassword(req.body.password, user.password, (err, isMatch) => {
        if (isMatch) {
          const userInfo = {
            user_id: user["_id"],
            username: user.username
          }
          const token = jwt.sign(userInfo, process.env.PASSJWT);
          utility.returnData(res, { ...userInfo, token }, "Se ha creado una sesión.")
        } else
          utility.returnError(res, 'FAILED-LOGIN', "Credenciales Inválidas", 401)
      })
    else
      utility.returnError(res, 'FAILED-LOGIN', "Credenciales Inválidas", 401)
  } catch (err) {
    console.log(err)
    utility.returnError(res, err)
  }
}

exports.loadJson = async () => {
  let userJson = fs.readFileSync(process.env.MAINDIR + '/resources/user.json');
  await User.deleteMany({})
  JSON.parse(userJson).map(element => {
    let new_user = new User({
      ...element
    });
    new_user.save();
  });
}