const fs = require('fs');
const Dealership = require('../models/dealership');
const mongoose = require('mongoose');

exports.loadJson = () => {
  return new Promise(async (resolve, reject) => {
    try {
      let dealershipJson = JSON.parse(fs.readFileSync(process.env.MAINDIR + '/resources/dealership.json'));
      await Dealership.deleteMany({})
      var countItem = 0
      dealershipJson.map(element => {
        let new_dealership = new Dealership({
          _id: new mongoose.Types.ObjectId(),
          ...element
        });
        new_dealership.save();
        countItem++;
        if (countItem == dealershipJson.length) {
          setTimeout(() => {
            resolve()
          }, 500)
        }
      });
    } catch (err) {
      reject(err)
    }
  })
}
