const fs = require('fs');
const Customer = require('../models/customer');
const Dealership = require('../models/dealership');
const utility = require('../services/utilities');
const mongoose = require('mongoose');

exports.getAll = async (req, res) => {
  try {
    const result = await Customer.find({}).populate({
      path: "dealership",
      model: "Dealership"
    })
    utility.returnData(res, result)
  } catch (err) {
    console.log(err)
    utility.returnError(res, err)
  }
}

exports.getById = async (req, res) => {
  try {
    const result = await Customer.findOne({ _id: new mongoose.Types.ObjectId(req.params.id) }).populate({
      path: "dealership",
      model: "Dealership"
    })
    utility.returnData(res, result)
  } catch (err) {
    utility.returnError(res, err)
  }
}

exports.new = async (req, res) => {
  try {
    await newRecort(req.body)
    utility.returnData(res, null, "El registro fue creado correctamente.")
  } catch (err) {
    utility.returnError(res, err)
  }
}

exports.update = async (req, res) => {
  try {
    const dealership = await Dealership.findOne({ name_code: req.body.dealership_code })
    await Customer.findOneAndUpdate({ _id: new mongoose.Types.ObjectId(req.params.id) }, {
      ...req.body,
      dealership: new mongoose.Types.ObjectId(dealership._id)
    })
    utility.returnData(res, null, "El registro fue actualizado correctamente.")
  } catch (err) {
    console.log(err)
    utility.returnError(res, err)
  }
}

exports.remove = async (req, res) => {
  try {
    await Customer.remove({ _id: new mongoose.Types.ObjectId(req.params.id) })
    utility.returnData(res, null, "El registro fue eliminado correctamente.")
  } catch (err) {
    utility.returnError(res, err)
  }
}

exports.search = async (req, res) => {
  let searchText = utility.prepareText(req.params.text)
  const queryText = await textQuery(searchText)
  const customers = await Customer.find(queryText).populate({
    path: "dealership",
    model: "Dealership"
  })
  utility.returnData(res, customers)
}

const textQuery = (textSearch) => {
  return new Promise((resolve, reject) => {
    const textArray = textSearch.split(" ");

    const fieldArray = ["name", "last_name", "username", "email", "address"];

    let queryArray = [];
    textArray.forEach((item, index, array) => {
      fieldArray.forEach((item2, index2, array2) => {
        queryArray.push({
          [item2]: {
            $regex: new RegExp(item + '.*', "i")
          }
        });
        if (index === array.length - 1 && index2 === array2.length - 1) {
          resolve({
            $or: queryArray
          });
        }
      });
    });
    reject();
  });
}

exports.loadJson = async () => {
  let customerJson = fs.readFileSync(process.env.MAINDIR + '/resources/customer.json');
  await Customer.deleteMany({})
  JSON.parse(customerJson).map(async element => {
    try {
      newRecort(element)
    } catch (err) {
      console.log(err)
    }
  });
}

const newRecort = async (item) => {
  return new Promise(async (resolve, reject) => {
    let new_customer = new Customer({
      ...item
    });
    try {
      const dealership = await Dealership.findOne({ name_code: item.dealership_code })
      new_customer.dealership = new mongoose.Types.ObjectId(dealership._id)
      await new_customer.save();
      resolve()
    } catch (err) {
      reject(err)
    }
  })
}