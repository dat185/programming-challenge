import { authHeader, handleResponse } from '../_helpers';

const getAll = () => {
    const requestOptions = { method: 'GET', headers: authHeader() };
    return fetch(`http://localhost:8080/api/v1/customer`, requestOptions).then(handleResponse);
}

export const userService = {
    getAll
};