import React from 'react';
import { Table, Badge } from 'react-bootstrap';
import { userService, authenticationService } from '../_services';

class HomePage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            currentUser: authenticationService.currentUserValue,
            users: null,
            textSearch: null
        };
    }

    componentDidMount() {
        userService.getAll().then(users => this.setState({ users }));
    }

    render() {
        const { users } = this.state;
        return (
            <div className="col-md-12">
                <h1>Empresa de Automóviles</h1>
                <p>Reto de Programación - CONSULTEC</p>
                <h3>Clientes y sus consecionarias</h3>

                {users &&
                    <Table striped bordered hover className="mt-3">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Apellido</th>
                                <th>Email</th>
                                <th>Dirección</th>
                                <th>Consesionaria</th>
                                <th>Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                            {users.map((item, key) => {
                                return <tr key={key}>
                                    <td>{item.name}</td>
                                    <td>{item.last_name}</td>
                                    <td>{item.email}</td>
                                    <td>{item.address}</td>
                                    <td>{item.dealership.name}</td>
                                    <td>{item.status === 'ENABLED' ? (<Badge variant="success">Habilitado</Badge>) :
                                        (<Badge variant="warning">Deshabilitado</Badge>)}</td>
                                </tr>
                            })}
                        </tbody>
                    </Table>
                }
            </div >
        );
    }
}

export { HomePage };