const express = require('express')
const app = express();
const logger = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const session = require('./services/session')
const cors = require('cors')
const methodOverride = require('method-override');
const utility = require('./services/utilities')
const swaggerUi = require('swagger-ui-express'),
  swaggerDocument = require('./resources/swagger.json');
require('dotenv').config()
process.env.MAINDIR = __dirname

app.use(cors())

app.use(methodOverride());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use('/api/v1', session.validation, require('./routes/user'));
app.use('/api/v1/customer', session.validation, require('./routes/customer'));
app.use(express.static('./front-end/build'));

mongoose.connect('mongodb://localhost:27017/challenge', {
  useUnifiedTopology: true,
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false
});

app.use((req, res) => {
  utility.returnError(res, "Endpoint not found.")
});

require('./services/init')()

module.exports = app;
